#pragma once

#include "Connection.h"

enum AuctionState
{
   STATE_INITIAL,
   STATE_BLOCKED_ON_READ,
   STATE_FILTERING,
   STATE_BLOCKED_ON_FILTER,
   STATE_RESPONSE_SENT
};



class Auction
{
public:
   Auction(Connection&& conn);
   ~Auction();

   bool hasBid() const;
   bool isBlocked() const;
   void proceed();

private:
   void transitionState();

   Connection m_conn;
   AuctionState m_state;
   std::string m_body;
};
