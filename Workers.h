#pragma once

#include <thread>
#include <vector>


class AuctionController;


class Workers
{
public:

   Workers(AuctionController& auctionController, int threads);
   ~Workers();

private:
   std::vector<std::thread> m_workers;
};
