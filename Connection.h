#pragma once

#include <utility>
#include <string>

class Connection
{
public:

   Connection();
   Connection(int fd);
   Connection(Connection&& other) noexcept;
   ~Connection();

   bool readBody(std::string& body);
   bool write(const std::string& text);

private:
   void setNonBlocking();

   int m_fd;
};
