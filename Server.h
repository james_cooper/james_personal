#pragma once

#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <cerrno>
#include <cstring>

#include <stdexcept>

class AuctionController;


class Server
{
public:
   Server(AuctionController&);

   void start(unsigned short port, unsigned int listenBacklog);

   template <typename T>
   static void setOption(int fd, int family, int option, T value)
   {
      int err = ::setsockopt(fd, family, option, &value, sizeof(T));
      if (err != 0)
      {
         throw std::runtime_error(::strerror(errno));
      }
   }

private:
   void createServerSocket(unsigned short port);
   void bindAndListen(unsigned int listenBacklog);
   static void throwErrnoIf(bool condition);

   AuctionController& m_auctionController;
   bool m_started;
   int m_listenFd;
   struct sockaddr_in m_servAddr;
};
