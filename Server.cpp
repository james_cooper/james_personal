#include <iostream>

#include "Server.h"
#include "AuctionController.h"
#include "Connection.h"

Server::Server(AuctionController& auctionController)
:  m_auctionController(auctionController),
   m_started(false),
   m_listenFd(-1)
{
}


void Server::throwErrnoIf(bool condition)
{
   if (condition)
   {
      throw std::runtime_error(::strerror(errno));
   }
}


void Server::start(unsigned short port, unsigned int listenBacklog)
{
   if (!m_started)
   {
      createServerSocket(port);
      bindAndListen(listenBacklog);
   }
}


void Server::createServerSocket(unsigned short port)
{
   memset(&m_servAddr, '\0', sizeof(m_servAddr));

   m_listenFd = ::socket(AF_INET, SOCK_STREAM, 0);
   throwErrnoIf(m_listenFd == -1);

   m_servAddr.sin_family = AF_INET;
   m_servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
   m_servAddr.sin_port = htons(port);
}


void Server::bindAndListen(unsigned int listenBacklog)
{
   setOption<int>(m_listenFd, SOL_SOCKET, SO_REUSEADDR, 1);
   setOption<int>(m_listenFd, SOL_SOCKET, SO_KEEPALIVE, 1);
   setOption<int>(m_listenFd, IPPROTO_TCP, TCP_NODELAY, 1);
   setOption<int>(m_listenFd, IPPROTO_TCP, TCP_DEFER_ACCEPT, 1);

   int err = ::bind(m_listenFd, (struct sockaddr*)&m_servAddr, sizeof(m_servAddr));
   throwErrnoIf(err != 0);

   err = ::listen(m_listenFd, listenBacklog);
   throwErrnoIf(err != 0);

   m_started = true;
   while (true)
   {
      int connFd = ::accept(m_listenFd, (struct sockaddr*)NULL, NULL);
      throwErrnoIf(connFd == -1);

      setOption(connFd, SOL_SOCKET, SO_REUSEADDR, 1);
      setOption(connFd, SOL_SOCKET, SO_KEEPALIVE, 1);

      if (m_auctionController.handleAccept(connFd) == false)
      {
         ::close(connFd);
      }
   }

   m_started = false;
}
