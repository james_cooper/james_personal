#include <xmmintrin.h>
#include <iostream>

#include "Workers.h"

#include "AuctionController.h"


inline static void BusySpin(unsigned int count)
{
   while (count--)
   {
      _mm_pause();
   }
}


Auction* ExponentialBackoff(AuctionController& auctionController, unsigned int limit)
{
   Auction* auction = auctionController.getNextAuction();
   if (auction)
   {
      return auction;
   }
   
   for (unsigned int backoff = 1; backoff < limit; backoff <<= 1)
   {
      BusySpin(backoff);
      auction = auctionController.getNextAuction();
      if (auction)
      {
         return auction;
      }
   }
   
   return nullptr;
}


Auction* GetNextAuction(AuctionController& auctionController, unsigned int spins)
{
   Auction* auction = nullptr;
   
   for (;;)
   {
      auction = ExponentialBackoff(auctionController, spins);
      if (auction)
      {
         return auction;
      }
      
      std::this_thread::yield();
   }
   
   return auction;
}


void WorkerThreadFunc(AuctionController& auctionController)
{
   try
   {
      unsigned int spins = 0x8000;
      
      while (true)
      {
         std::unique_ptr<Auction> auction(GetNextAuction(auctionController, spins));
         auction->proceed();

         if (auction->isBlocked())
         {
            auctionController.addAuction(auction.get());
            auction.release();
         }
      }
   }
   catch (const std::exception& e)
   {
      std::cerr << "*** WorkerThreadFunc exception: " << e.what() << "\n";
   }
}



Workers::Workers(AuctionController& auctionController, int threads)
{
   for (int i = 0; i < threads; ++i)
   {
      m_workers.push_back(std::thread(WorkerThreadFunc, std::ref(auctionController)));
   }
}


Workers::~Workers()
{
   for (auto& t : m_workers)
   {
      if (t.joinable())
      {
         t.join();
      }
   }
}
