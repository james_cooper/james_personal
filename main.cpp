#include <iostream>

#include "AuctionController.h"
#include "Server.h"
#include "Workers.h"

int main()
{
   const int threads = std::thread::hardware_concurrency();
   const unsigned int port = 8080;
   const unsigned int listenBacklog = 1024;

   try
   {
      AuctionController controller;
      Server server(controller);
      Workers workers(controller, threads);      
      
      std::cout << "Server listening on port: " << port << "\n";
      std::cout << "Server listen backlog: " << listenBacklog << "\n";
      std::cout << "Server is spawning " << threads << " workers\n";
      
      server.start(port, listenBacklog);
   }
   catch (const std::exception& e)
   {
      std::cerr << "Exception: " << e.what() << "\n";
   }
   catch (...)
   {
      std::cerr << "Exception: unknown\n";
   }

   return 0;
}

