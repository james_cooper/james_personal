#include <memory>
#include <iostream>

#include "AuctionController.h"


AuctionController::AuctionController()
{
}


bool AuctionController::handleAccept(Connection&& conn)
{
   try
   {
      std::unique_ptr<Auction> auction(new Auction(std::forward<Connection>(conn)));
      m_readyQ.push(auction.get());
      auction.release();

      return true;
   }
   catch (const std::exception& e)
   {
   }

   return false;
}


Auction* AuctionController::getNextAuction()
{
   Auction* auction = nullptr;

   m_readyQ.try_pop(auction);

   return auction;
}


void AuctionController::addAuction(Auction* auction)
{
   if (auction)
   {
      m_readyQ.push(auction);
   }
}
