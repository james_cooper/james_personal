#include <iostream>

#include "Auction.h"


Auction::Auction(Connection&& conn)
:  m_conn(std::move(conn)),
   m_state(STATE_INITIAL)
{
}


Auction::~Auction()
{
}


bool Auction::hasBid() const
{
   return m_state == STATE_RESPONSE_SENT;
}


void Auction::proceed()
{
   do
   {
      transitionState();
   }
   while (!isBlocked() && !hasBid());
}


void Auction::transitionState()
{
   switch (m_state)
   {
   case STATE_INITIAL:
      m_body.clear();
      m_state = (m_conn.readBody(m_body) ? STATE_FILTERING : STATE_BLOCKED_ON_READ);
      break;

   case STATE_BLOCKED_ON_READ:
      m_state = (m_conn.readBody(m_body) ? STATE_FILTERING : STATE_BLOCKED_ON_READ);
      break;

   case STATE_FILTERING:
      m_conn.write("HelloWorld");
      m_state = STATE_RESPONSE_SENT;
      return;

   case STATE_BLOCKED_ON_FILTER:
      break;
   }
}


bool Auction::isBlocked() const
{
   switch (m_state)
   {
   case STATE_BLOCKED_ON_READ:
      return true;

   default:
      return false;
   }
}
