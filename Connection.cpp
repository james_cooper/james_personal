#include <iostream>
#include <algorithm>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>  // strerror
#include "Connection.h"


Connection::Connection()
:  m_fd(-1)
{
   setNonBlocking();
}


Connection::Connection(int fd)
:  m_fd(fd)
{
   setNonBlocking();
}


Connection::Connection(Connection&& other) noexcept
:  Connection()
{
   std::swap(m_fd, other.m_fd);
}


Connection::~Connection()
{
   if (m_fd != -1)
   {
      ::close(m_fd);
   }
}


void Connection::setNonBlocking()
{
   //O_NONBLOCK;
   if (m_fd != -1)
   {
      int flags = ::fcntl(m_fd, F_GETFL);
      flags |= O_NONBLOCK;
      ::fcntl(m_fd, F_SETFL, flags);
   }
}


bool Connection::readBody(std::string& body)
{
   const int BUFFER_SIZE = 1024;
   
   char buffer[BUFFER_SIZE];
   int flags = MSG_DONTWAIT | MSG_NOSIGNAL;
   int err;

   while ((err = ::recv(m_fd, buffer, BUFFER_SIZE, flags)) != 0)
   {
      if (err == -1)
      {
         if ((errno == EAGAIN) || (errno == EWOULDBLOCK))
         {
            return false;
         }

         throw std::runtime_error(strerror(errno));
      }
      else if (err > 0)
      {
         body.append(buffer, err);
      }

      if (err < BUFFER_SIZE)
      {
         return true;
      }
   }

   return true;
}


bool Connection::write(const std::string& text)
{
   int flags = MSG_NOSIGNAL;

   ::send(m_fd, text.c_str(), text.length(), flags);

   return true;
}
