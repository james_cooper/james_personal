#pragma once

#include <tbb/concurrent_queue.h>

#include "Auction.h"


class AuctionController
{
public:
   AuctionController();

   bool handleAccept(Connection&& conn);
   Auction* getNextAuction();
   void addAuction(Auction* auction);

private:
   tbb::concurrent_queue<Auction*> m_readyQ;
};
